#ifndef __FRAME_H__
#define __FRAME_H__

#include <opencv2/objdetect/objdetect.hpp>

#define MIN_SIZE 20

namespace blob {

    class Frame
      {
    public :
      Frame( int c0, int r0, int c1, int r1 ) : c0(c0), r0(r0), c1(c1), r1(r1) { }

      bool update( cv::Mat f, int threshold, double min_ratio, double max_ratio );

      void getCenter( int & col, int & row );

      void addToFrame( cv::Mat f, const cv::Scalar & color );

      void moveToward( int col, int row );

      friend std::ostream & operator<< ( std::ostream & os, const Frame & f );

    public:

      int c0,r0,c1,r1;

      };

}

#endif
