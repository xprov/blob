#include <iostream>
#include <opencv2/imgproc.hpp>
#include "Frame.h"


using namespace std;
using namespace cv;
using namespace blob;

// Frame::Frame( int c0, int r0, int c1, int r1 )
//      : c0(c0), r0(r0), c1(c1), r1(r1)
// { }



bool Frame::update( cv::Mat f, int threshold, double min_ratio, double max_ratio ) 
{
  int channels = f.channels();
  int n0,n1,r,c;
  uchar *p0, *p1;
  double ratio0, ratio1;
  bool lost = true;

  if ( channels != 1 )
    {
      cerr << "Wrong channel number" << endl;
      exit(1);
    }

  // test top and bottom rows
  n0 = 0;
  n1 = 0;
  p0 = f.ptr<uchar>(r0);
  p1 = f.ptr<uchar>(r1);
  for ( c=c0; c<c1; ++c )
    {
      n0 += ( p0[c] >= threshold ) ? 1 : 0;
      n1 += ( p1[c] >= threshold ) ? 1 : 0;
    }
  ratio0 = static_cast<double>( n0 ) / static_cast<double>( c1-c0 );
  ratio1 = static_cast<double>( n1 ) / static_cast<double>( c1-c0 );
  if ( n0 + n1 > 0 )
    lost = false;

  if ( r1-r0 > MIN_SIZE )
    {
      r0 += ( ratio0 < min_ratio ) ?  1 : 0;
      r1 += ( ratio1 < min_ratio ) ? -1 : 0;
    }
  r0 += ( ratio0 > max_ratio ) ? -1 : 0;
  r1 += ( ratio1 > max_ratio ) ?  1 : 0;

  // test top and bottom columns
  n0 = 0;
  n1 = 0;
  for ( r=r0; r<r1; ++r )
    {
      p0 = f.ptr<uchar>(r);
      n0 += ( p0[c0] > threshold ) ? 1 : 0;
      n1 += ( p0[c1] > threshold ) ? 1 : 0;
    }
  ratio0 = static_cast<double>( n0 ) / static_cast<double>( r1-r0 );
  ratio1 = static_cast<double>( n1 ) / static_cast<double>( r1-r0 );
  if ( n0 + n1 > 0 )
    lost = false;


  if ( c1-c0 > MIN_SIZE )
    {
      c0 += ( ratio0 < min_ratio ) ?  1 : 0;
      c1 += ( ratio1 < min_ratio ) ? -1 : 0;
    }

  c0 += ( ratio0 > max_ratio ) ? -1 : 0;
  c1 += ( ratio1 > max_ratio ) ?  1 : 0;

  if ( c0 < 0 )
    c0 = 0;
  if ( r0 < 0 )
    r0 = 0;
  if ( c1 > f.cols )
    c1 = f.cols - 1;
  if ( r1 > f.rows )
    r1 = f.rows - 1;

  // avoid warning for ``lost```not being used.
  return lost;
}

void Frame::getCenter( int & col, int & row )
{
  col = ( c1+c0 ) / 2;
  row = ( r1+r0 ) / 2;
}


void Frame::addToFrame( Mat f, const Scalar & color )
{
  cv::rectangle( f, Point( c0, r0 ), Point( c1, r1 ), color );
  //circle( f, Point(c0,r0), 3, Scalar( 255, 0, 0 ), 3, 8, 0 );
  //circle( f, Point(c1,r1), 3, Scalar( 255, 0, 0 ), 1, 3, 0 );
}


void Frame::moveToward( int col, int row )
{
  if ( ( c0 < col ) && ( c1 < col ) )
    {
      ++c0; ++c1;
    }
  if ( ( c0 > col ) && ( c1 > col ) )
    {
      --c0; --c1;
    }
  if ( ( r0 < row ) && ( r1 < row ) )
    {
      ++r0; ++r1;
    }
  if ( ( r0 > row ) && ( r1 > row ) )
    {
      --r0; --r1;
    }
}

ostream & operator<< ( ostream & os, const Frame & f )
{
  os << "[Frame] " << f.c0 << " " << f.r0 << " " << f.c1 << " " << f.r1;
  return os;
}
