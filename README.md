# Blob #

Blob is a little game where you discover an image. The image is covered by
discs which split into smaller discs when touched.

Yes, the idea is totally taken from the nice [www.koalastothemax.com](https://www.koalastothemax.com)

The funny part is that the player has to move in order to control the game.
Images from the webcam are analyse through a motion detection process. This
whole process is broken down into simple steps and a basic interface allows to
customize every parameter and visualized the result.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![blobdemo.gif](./demo/blobdemo.gif)



## Setup ##

 -  Dependencies: `OpenCV`, `Gtk3`, `cairo`.

#### Using `cmake` (recommended)


 -  __Compilation__

	```
	mkdir build
	cd build
	cmake ..
	make
	```

 - __Run__

	From the `build` folder
	```
	./blob [path to image] [option]
	```

#### Note ####

OpenCV might display the following warnings
```
[ WARN:0] global ../modules/videoio/src/cap_gstreamer.cpp (1824) handleMessage OpenCV | GStreamer warning: Embedded video playback halted; module source reported: Could not read from resource.
[ WARN:0] global ../modules/videoio/src/cap_gstreamer.cpp (914) open OpenCV | GStreamer warning: unable to start pipeline
[ WARN:0] global ../modules/videoio/src/cap_gstreamer.cpp (501) isPipelinePlaying OpenCV | GStreamer warning: GStreamer: pipeline have not been created
```

In such case, add the following line to your .bashrc
```
export OPENCV_LOG_LEVEL=ERROR
```


