#include "imgTree.h"

using namespace blob;


namespace blob {
    std::ostream& operator<<( std::ostream& os, Node & n )
      {
        os << "Node : (" << n.c0 << "," << n.r0 << ") to (" << n.c1 << "," << n.r1 
          << "), color=(" << (int) n.color.val[0] << "," 
          << (int) n.color.val[1] << "," 
          << (int) n.color.val[2]
          << "), closed=" << n.closed 
          << ", final=" << n.final 
          << ", sons=[" << n.sons[0] << "," << n.sons[1] 
          << "," << n.sons[2] << "," << n.sons[3] << "]"
          << std::endl;
        return os;
      }
}

imgTree::imgTree( const cv::Mat& img, int min_radius ) : theImage(img), minimal_radius(min_radius)
{
  buildTree( );
  visible.insert( root );
}

inline
cv::Vec3b couleur_moyenne( Node * tab[4] )
{
  cv::Vec3b c;
  int tmp;
  for ( int k=0; k<3; ++k )
    {
      tmp = 0;
      for ( int l=0; l<4; ++l )
        {
          tmp += tab[l]->color.val[k];
        }
      c.val[k] = tmp / 4;
    }
  return c;
}

inline 
cv::Vec3b couleur_moyenne( cv::Mat m, int i0, int j0, int i1, int j1 )
{
  int tmp[3];
  for ( int k=0; k<3; ++k )
    {
      tmp[k] = 0.0;
      for ( int i=i0; i<i1; ++i )
        for ( int j=j0; j<j1; ++j )
          tmp[k] += m.at<cv::Vec3b>(i,j).val[k];
    }
  cv::Vec3b cm;
  for ( int k=0; k<3; ++k )
    cm.val[k] = tmp[k] / ( (i1-i0)*(j1-j0) );
  return cm;
}

Node * imgTree::buildNode( int i0, int j0, int i1, int j1 )
{
  Node * nouveau;
  int rstep = i1-i0;
  int cstep = j1-j0;
  if ( std::min( rstep, cstep ) <= minimal_radius )
    {
      cv::Vec3b couleur = couleur_moyenne( theImage, i0, j0, i1, j1 );
      nouveau = new Node( i0, j0, i1, j1, true, couleur );
      for ( int i=0; i<4; ++ i)
        nouveau->sons[i] = NULL;
    }
  else
    {
      Node * sons[4];
      sons[0] = buildNode( i0, j0, i0+(rstep/2), j0+(cstep/2) );
      sons[1] = buildNode( i0, j0+(cstep/2), i0+(rstep/2), j1 );
      sons[2] = buildNode( i0+(rstep/2), j0, i1, j0+(cstep/2) );
      sons[3] = buildNode( i0+(rstep/2), j0+(cstep/2), i1, j1 );
      cv::Vec3b couleur = couleur_moyenne( sons );
      nouveau = new Node( i0, j0, i1, j1, false, couleur );
      for ( int i=0; i<4; ++ i)
        nouveau->sons[i] = sons[i];
    }
  return nouveau;
}

void imgTree::buildTree( ) 
{
  root = buildNode( 0, 0, theImage.rows, theImage.cols );
  return ;

  Node * matrix[ theImage.rows ][ theImage.cols ];
  for ( int i=0; i<theImage.rows; ++i )
    for ( int j=0; j<theImage.cols; ++j )
      {
        cv::Vec3b c = theImage.at<cv::Vec3b>(i,j);
        matrix[i][j] = new Node( i, j, i, j, true, c );
      }
  int step = 1;
  while ( step < std::max( theImage.rows, theImage.cols ) )
    {
      for ( int i=0; i<theImage.rows; i += step )
        for ( int j=0; j<theImage.cols; j += step )
          {
            if ( ( i+step < theImage.rows ) && ( j+step < theImage.cols ) )
              {
                cv::Vec3b c;
                cv::Vec3b c1 = matrix[i][j]->color;
                cv::Vec3b c2 = matrix[i+step][j]->color;
                cv::Vec3b c3 = matrix[i][j+step]->color;
                cv::Vec3b c4 = matrix[i+step][j+step]->color;
                for ( int k=0; k<3; ++k )
                  {
                    c.val[k] = c1.val[k]/4 + c2.val[k]/4 + c3.val[k]/4 + c4.val[k]/4;
                  }
                Node * newNode = new Node( i, j, i+2*step, j+2*step, false, c );
                newNode->sons[0] = matrix[i][j];
                newNode->sons[1] = matrix[i][j+step];
                newNode->sons[2] = matrix[i+step][j];
                newNode->sons[3] = matrix[i+step][j+step];
                matrix[i][j] = newNode;
              }
            else if ( i+step < theImage.rows )
              {
                cv::Vec3b c;
                cv::Vec3b c1 = matrix[i][j]->color;
                cv::Vec3b c2 = matrix[i+step][j]->color;
                for ( int k=0; k<3; ++k )
                  {
                    c.val[k] = c1.val[k]/2 + c2.val[k]/2;
                  }
                Node * newNode = new Node( i, j, i+2*step, theImage.cols-1, false, c );
                newNode->sons[0] = matrix[i][j];
                newNode->sons[1] = NULL;
                newNode->sons[2] = matrix[i+step][j];
                newNode->sons[3] = NULL;
                matrix[i][j] = newNode;
              }
            else  if ( j+step < theImage.cols )
              {
                cv::Vec3b c;
                cv::Vec3b c1 = matrix[i][j]->color;
                cv::Vec3b c2 = matrix[i][j+step]->color;
                for ( int k=0; k<3; ++k )
                  {
                    c.val[k] = c1.val[k]/2 + c2.val[k]/2;
                  }
                Node * newNode = new Node( i, j, theImage.rows-1, j+2*step, false, c );
                newNode->sons[0] = matrix[i][j];
                newNode->sons[1] = matrix[i][j+step];
                newNode->sons[2] = NULL;
                newNode->sons[3] = NULL;
                matrix[i][j] = newNode;
              }
          }
      step *= 2;
    }
  root = matrix[0][0];
}


void imgTree::blowNode( Node * n )
{
  if ( ! n->final )
    {
      bool leaf = true;
      for ( int i=0; i<4; ++i )
        {
          if ( n->sons[i] != NULL )
            {
              leaf = false;
              if ( n->sons[i]->radius > minimal_radius )
                {
                  visible.insert( n->sons[i] );
                }
            }
        }
      if ( !leaf )
        visible.erase( n );
    }
}


