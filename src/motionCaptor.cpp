/**
 * @file motionCaptor.cpp
 */

#include <iostream>
#include <stdio.h>
#include <thread>

#include "motionCaptor.h"

using namespace std;
using namespace cv;
using namespace blob;

/** Function Headers */
void display( Mat frame );

/** Global variables */
string window_name = "Capture - Motion detection";

#define NFRAMES 2
#define NB_FRAME_UPDATE 20

void motionCaptor::medval( Mat f, int & medR, int & medC )
{
  int nr = 0;
  int nc = 0;

  int channels = f.channels();
  int nRows = f.rows * channels;
  int nCols = f.cols;

  switch ( channels )
    {
    case 1 :
      for ( int r=0; r<nRows; ++r )
        {
          uchar * p = f.ptr<uchar>(r);
          for ( int c=0; c<nCols; ++c )
            {
              if ( p[c] > I.getTreshold() )
                {
                  medR += r;
                  ++nr;
                  medC += c;
                  ++nc;
                }
            }
        }
      break;
    case 3 :
      for ( int r=0; r<nRows; ++r )
        {
          Vec3b * p = f.ptr<Vec3b>(r);
          for ( int c=0; c<nCols; ++c )
            {
              if ( p[c][0] + p[c][1] + p[c][2] > I.getTreshold() )
                {
                  medR += r;
                  ++nr;
                  medC += c;
                  ++nc;
                }
            }
        }
      break;
    }
  if ( nr > 0 )
    medR /= nr;
  if ( nc > 0 )
    medC /= nc;
}

void motionCaptor::captor_func()
{
  Mat frame[NFRAMES];
  int i=1;
  bool first_time = true;
  Mat f;
  while( true )
    {
      //std::cout << "captor_func début iter" << std::endl;
      capture >> f;
      Mat originalFrame;
      //Mat originalFrame = f.clone();
      // Frame is flipped in order to product a "mirror-like" image.
      cv::flip( f, originalFrame, 1 );
      frame[i] = originalFrame.clone();

      cvtColor( frame[i], frame[i], cv::COLOR_RGB2GRAY, 1 );

      if ( I.sMask() > 0 )
        GaussianBlur( frame[i], frame[i], Size( I.sMask(), I.sMask() ), 2 );

      //Mat diff;
      Mat toAnalyse;
      if ( I.diffIsActivated() && !first_time )
        {
          absdiff( frame[i], frame[ (i==0) ? NFRAMES-1 : i-1 ], toAnalyse );
        }
      else
        {
          toAnalyse = frame[i].clone();
        }

      if ( I.tresholdIsActivated() )
        threshold( toAnalyse, toAnalyse, I.getTreshold(), 255, cv::THRESH_BINARY );


      if ( I.getDilatation() ) 
        dilate( toAnalyse, toAnalyse, Mat(), Point(-1,-1), I.getDilatation() );

      if ( I.getErosion() ) 
        erode( toAnalyse, toAnalyse, Mat(), Point(-1,-1), I.getErosion() );

      Mat toShow;
      toShow = ( I.simulationIsActivated() ) ?  originalFrame : toAnalyse;

      // Compute the center of all movement detected and add this new points to
      // the list.
      int r,c;
      medval( toAnalyse, r, c );

      // old analyses, the new position are stored as the average of the
      // detected movement.
      //mc->new_position( Point(c,r) );

      int r_avg, c_avg;
      Point2d p1, p2, p3;
      getActualPosition( p1, p2, p3 );
      c_avg = static_cast<int>( p1.x * cols );
      r_avg = static_cast<int>( p1.y * rows );

      // Update the frame around the moving area.
      for ( int k=0; k<NB_FRAME_UPDATE; ++k )
        {
          bool lost = myFrame->update( toAnalyse, 1, .0001, .1 );
          if ( lost )
            {
              myFrame->moveToward( c, r );
            }
        }

      // new analysis, the position stored is the center of the frame
      int r_tmp, c_tmp;
      myFrame->getCenter( c_tmp, r_tmp );
      new_position( Point( c_tmp, r_tmp ) );

      // Display tracking information on the webcam's images.
      if ( I.showFrame() )
        {
          myFrame->addToFrame( toShow, Scalar( 255, 255, 255 ) );
        }

      if ( I.showCenter() )
        {
          //cout << c << " " << r << " " << c_avg << " " << r_avg << endl; 
          circle( toShow, Point(c,r), 10, Scalar( 255, 0, 0 ), 2 );
          circle( toShow, Point(c_avg,r_avg), 10, Scalar( 0, 0, 255 ), 2 );

        }
      if ( I.displayWebcamActivated() ) 
        {
          Mat flipped;
          display( toShow ); 
        }

      //usleep(100000);
      //int key = waitKey(10) % 0x100;
      //if( (char)key == 27 ) { break; }
      //for ( int j=0; j < I->nIgnoredFrames(); ++j )
      //  {
      //    Mat f;
      //    f = cvQueryFrame( mc->capture );
      //    key = waitKey(10);
      //    if( (char)key == 'c' ) { break; }
      //  }
      i = (i+1) % NFRAMES;
      first_time = false;
      //std::cout << "capture_func iterator" << std::endl;
    }
}


motionCaptor::motionCaptor( int smoothness, bool interactive, shared_point * p, int caption_width, int caption_height ) : output_position(p), points_avg( smoothness ), lastPos(new Point[points_avg])
{
  output_position->write( Point2d( 10, 10 ) );
  I.Start(interactive);
  I.displayWebcam = interactive;

  pthread_mutex_init( &pos_array_protection, NULL );
  for ( int i=0; i<points_avg; ++i )
    {
      lastPos[i] = Point(0,0);
    }
  posInPos = 0;


  //-- 2. Read the video stream
  capture = cv::VideoCapture("/dev/video0");
  if (!capture.isOpened()) {
      std::cerr << "Unable ot open camera 0" << std::endl;
      exit(-1);
  }
  capture.set( cv::CAP_PROP_FRAME_WIDTH, caption_width );
  capture.set( cv::CAP_PROP_FRAME_HEIGHT, caption_height );
  capture.set( cv::CAP_PROP_FPS, CAPTION_FPS );

  
  cv::Mat f;
  capture >> f;
  rows = f.rows;
  cols = f.cols;

  myFrame = new Frame( 10, 10, cols-10, rows-10 );
  captor_func();
  //std::thread t(&motionCaptor::captor_func, *this);
}



void motionCaptor::display( Mat frame )
{
  imshow( window_name, frame );
  waitKey( CAPTION_FPS );
}



void motionCaptor::new_position( Point p )
{
  pthread_mutex_lock( &pos_array_protection );
  lastPos[ posInPos ] = p;
  posInPos = ( posInPos + 1 ) % points_avg;
  pthread_mutex_unlock( &pos_array_protection );
}



void motionCaptor::getActualPosition( Point2d & circle, Point2d & rectangle1, Point2d & rectangle2 )
{
  pthread_mutex_lock( &pos_array_protection );
  double x_avg = 0.0;
  double y_avg = 0.0;
  for ( int i=0; i<points_avg; ++i )
    {
      x_avg += (double) lastPos[i].x;
      y_avg += (double) lastPos[i].y;
    }
  pthread_mutex_unlock( &pos_array_protection );
  //circle = Point2d( 1.0 - (x_avg / (double) ( points_avg * cols  )), 
  //                y_avg / (double) ( points_avg * rows ) );
  circle = Point2d( x_avg / (double) ( points_avg * cols  ), 
                    y_avg / (double) ( points_avg * rows ) );
  getFrameCorners( rectangle1, rectangle2 );

  // Communicate what seems to be the best position for the movement detection !
  output_position->write( circle );
}

void motionCaptor::getFrameCorners( Point2d & top_left, Point2d & bottom_right )
{
  double c = 1.0 - ( static_cast<double>( myFrame->c1 )/static_cast<double>( cols ) );
  double r = static_cast<double>( myFrame->r0 )/static_cast<double>( rows );
  top_left = Point2d( c, r );

  c = 1.0 - ( static_cast<double>( myFrame->c0 )/static_cast<double>( cols ) );
  r = static_cast<double>( myFrame->r1 )/static_cast<double>( rows );
  bottom_right = Point2d( c, r );
}

