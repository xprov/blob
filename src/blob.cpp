#include <iostream>
#include <unistd.h>
#include <stdlib.h>
//#include <math.h>
#include <time.h>
#include <sys/wait.h>
#include <cairo/cairo.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "imgTree.h"
#include "motionCaptor.h"

#define DEFAULT_MIN_CIRCLE_RADIUS 5
#define DEFAULT_SMOOTHNESS 2
#define DEFAULT_INTERACTIVE_DETECTION false
#define DEFAULT_DISPLAY_INTRO true
#define REFRESH_RATE 30
#define START_DELAY 3
#define INITIAL_DELAI 2
#define DEFAULT_CAPTION_WIDTH 500
#define DEFAULT_CAPTION_HEIGHT 500

#define TMP_PNG_IMAGE_NAME ".tmp.png"

using namespace blob;


void usage( const char * nom )
{
  std::cerr << "Usage : " << nom << " [image] [options]\n";
  std::cerr << "Options : \n";
  std::cerr << "  -m [taille] : spécifie la taille (en pixels) du rayon minimal des cercles (défaut=15).\n";
  std::cerr << "  -s [entier] : la \"smoothness\" avec laquelle le point se déplace (défaut=15).\n";
  std::cerr << "  -i : Affiche les images saisies par la webcam et active le menu interactif de détection des mouvements.\n";
  std::cerr << "  -c [entier] [entier] : Taille souhaitée ( mais pas nécessairement obtenue ) des images lues par la webcam. ( défault : 500 500 )\n";
  std::cerr << "  Si aucune image n'est spécifiée, ``../images/mystere`` sera utilisée.\n";
  std::cerr << std::endl;
}


/**
 * Data for graphic display.
 */
typedef struct
{
  GtkWidget * drawing_area;
  cairo_surface_t * image;
  imgTree * myTree;
  shared_point position;
} Data; 


cv::Point2d relativeToAbsolute( Data * D, cv::Point2d relativePosition, double dilatation )
{
  cv::Point2d center( 0.5, 0.5 );
  cv::Point2d diff = relativePosition - center;
  diff.x *= dilatation;
  if ( diff.x < -0.5 )
    diff.x = -0.5;
  else if ( diff.x > 0.5 )
    diff.x = 0.5;
  diff.y *= 1.25;
  if ( diff.y < -0.5 )
    diff.y = -0.5;
  else if ( diff.y > 0.5 )
    diff.y = 0.5;
  relativePosition = center + diff;
  return cv::Point2d( relativePosition.x * D->myTree->ncols(), 
                      relativePosition.y * D->myTree->nrows() );
}


/**
 * Display
 *
 * The image is displayed as circles of different sizes whose color is the
 * average color of the underneath pixels.
 *
 * When the dot hits a circle its splitted in smaller circles until the minimum
 * size is reached, at which point the original image is displayed.
 *
 * The circles and their splittings are managed in the imtTree class.
 */
void draw( cairo_t *cr, Data * D )
{
  // acts as a timer so that as the beggining the player seens just one big
  // blob.
  static int nbCalls = 0;
  nbCalls++;
  bool too_soon = nbCalls < START_DELAY*REFRESH_RATE;

  // paint the image as a background
  cairo_set_source_surface( cr, D->image, 0, 0 );
  cairo_paint(cr);

  // Position of the dot
  cv::Point2d rel_pos = D->position.read();
  cv::Point2d position = relativeToAbsolute( D, rel_pos, 1.2 );

  // draw the blobs
  //
  // When a blob in blown into smaller blobs, it is likely that the dot is
  // locate in one of the smaller blobs. We don't want this blob to be blown for
  // as long as the dot haven't got out of it.
  // toIgnore keeps track of the smaller blob.
  static Node* toIgnore = NULL; 
  if ( toIgnore != NULL && !toIgnore->isInside(position) ) {
      toIgnore = NULL;
  }

  // Copy the set of visible nodes because it might be modified whithing the
  // loop
  std::set<Node*> visible = std::set<Node*>(*D->myTree->getVisible());
  std::set<Node*>::const_iterator it = visible.begin();
  std::set<Node*>::const_iterator itEnd = visible.end();
  for( ; it != itEnd; ++it)
    {
      Node * n = *it;
      if ( !too_soon && n != toIgnore && n->isInside( position ) )
        {
          D->myTree->blowNode( n );
          for ( int i=0; i<4; ++i )
            {
              if ( ( n->sons[i] != NULL ) && ( n->sons[i]->isInside( position ) ) )
                {
                  toIgnore = n->sons[i];
                }
            }
        }

      cairo_set_source_rgb( cr, 1.0, 1.0, 1.0 );
      cairo_rectangle( cr, n->c0, n->r0, n->c1 - n->c0 + 1, n->r1 - n->r0 + 1 );
      cairo_fill(cr);

      cairo_set_source_rgb( cr, 
                           n->color.val[2] / 255.0, 
                           n->color.val[1] / 255.0, 
                           n->color.val[0] / 255.0 );
      cairo_arc(cr, n->center.x, n->center.y, n->radius, 0, 2 * M_PI);
      cairo_fill(cr);
    }

  cairo_set_source_rgb( cr, 1.0, 0.0, 0.0 ); 
  cairo_arc( cr, position.x, position.y, 3, 0, 2*M_PI );
  cairo_fill( cr );

  cairo_set_source_rgb( cr, 0.0, 0.0, 1.0 );
  cairo_stroke(cr);
}



/////////////////////////
//
// GTK callback (begin)
//

gboolean on_draw_event( GtkWidget *widget, cairo_t *cr, Data * D )
{
  (void) widget;
  draw(cr, D);
  return TRUE;
}



static gboolean delete_event( )
{
  unlink( TMP_PNG_IMAGE_NAME );
  gtk_main_quit ();
  return gboolean( true );
}


gboolean timeout(GtkWidget *widget)
{
    gtk_widget_queue_draw(widget);
    return TRUE;
}

//
// GTK callback (end)
//
/////////////////////////

int main( int argc, char ** argv )
{

  Data D;

  int min_circle_radius = DEFAULT_MIN_CIRCLE_RADIUS;
  int smoothness = DEFAULT_SMOOTHNESS;
  bool interactive_detection = DEFAULT_INTERACTIVE_DETECTION;
  int caption_width = DEFAULT_CAPTION_WIDTH;
  int caption_height = DEFAULT_CAPTION_HEIGHT;

  /*******************************************************
   * Lecture des arguments de la ligne de commande.
   */

  int i=1;
  const char * image_filename = "../images/mystere";
  while ( i < argc )
    {
      if ( argv[i][0] != '-' )
        {
          image_filename = argv[i];
          ++i;
        }
      else if ( std::string(argv[i]).compare( std::string( "-m" ) ) == 0 )
        {
          min_circle_radius = atoi( argv[i+1] );
          i += 2;
        }
      else if ( std::string(argv[i]).compare( std::string( "-s" ) ) == 0 )
        {
          smoothness = atoi( argv[i+1] );
          i += 2;
        }
      else if ( std::string(argv[i]).compare( std::string( "-i" ) ) == 0 )
        {
          interactive_detection = ! interactive_detection;
          i += 1;
        }
      else if ( std::string(argv[i]).compare( std::string( "-c" ) ) == 0 )
        {
          caption_width = atoi( argv[i+1] );
          caption_height = atoi( argv[i+2] );
          i += 3;
        }
      else 
        {
          usage( argv[0] );
          exit( EXIT_FAILURE );
        }
    }

  if ( image_filename == NULL )
    {
      usage( argv[0] );
      exit( EXIT_FAILURE );
    }

  std::cout << "#########################\n";
  std::cout << "# Minimal circle radius : " << min_circle_radius << "\n";
  std::cout << "# Smoothness : " << smoothness << "\n";
  std::cout << "# Interactive detection : " << interactive_detection << "\n";
  std::cout << "# Caption size : " << caption_width << "x" << caption_height << "\n";
  std::cout << "#########################\n" << std::endl;


  /*
   *******************************************************/

  /*******************************************************
   * Lecture de l'image par openCV
   */

  // opencv gère a peu près n'importe quelle image...
  // Cairo, exige un png... on va donc convertir.
  cv::Mat img = cv::imread( image_filename, cv::IMREAD_COLOR );
  cv::imwrite( TMP_PNG_IMAGE_NAME, img );
  imgTree myTree( img, min_circle_radius );
  D.myTree = &myTree;


  /*
   *******************************************************/



  int pid = fork();


  if ( pid > 0 )
    {

      /*******************************************************
       * Capture des mouvements
       */
      motionCaptor myMc( smoothness, interactive_detection, &D.position, caption_width, caption_height );
      /*
       *******************************************************/
      wait( NULL );
    }
  else
    {

      /*******************************************************
       * Interface GTK
       */

      D.image = cairo_image_surface_create_from_png( TMP_PNG_IMAGE_NAME );

      // Only two widgets are used, the drawing_area goes inside the windows
      GtkWidget *window, *drawing_area;

      gtk_init (&argc, &argv);
      window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
      drawing_area = gtk_drawing_area_new(); 
      gtk_container_add ( GTK_CONTAINER(window),  drawing_area );
      //gtk_widget_set_size_request( drawing_area, myTree.ncols(), myTree.nrows() );

      g_signal_connect( G_OBJECT( drawing_area ), "draw",
                       G_CALLBACK( on_draw_event ), (gpointer) &D );
      g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
      g_signal_connect (window, "delete-event", G_CALLBACK (delete_event), NULL);

      //Set window position, default size, and title.
      gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
      gtk_window_set_default_size(GTK_WINDOW(window), myTree.ncols(), myTree.nrows());
      gtk_window_set_title(GTK_WINDOW(window), "Blob");

      g_timeout_add(1000 / REFRESH_RATE, (GSourceFunc)timeout, window);
      gtk_widget_show_all( window );

      D.drawing_area = drawing_area;

      gtk_main();

      /*
       *******************************************************/
    }

  return 0;
}



