
#ifndef __MOTION_INTERFACE_C__
#define __MOTION_INTERFACE_C__

#include <iostream>
#include <pthread.h>

namespace blob {

    class motionInterface
      {
    public :

      motionInterface();
      ~motionInterface();

      void Start( bool interactive );

      bool tresholdIsActivated()
        {
          return tresholdActivated;
        }


      int sMask()
        {
          return sizeOfMask;
        }

      bool diffIsActivated()
        {
          return diffActivated;
        }

      int nIgnoredFrames()
        {
          return numberOfIgnoredFrames;
        }

      int getTreshold()
        {
          return treshold;
        }

      bool showCenter()
        {
          return centerActivated;
        }

      int getDilatation()
        {
          return theDilatation;
        }

      int getErosion()
        {
          return theErosion;
        }

      bool simulationIsActivated()
        {
          return simulationActivated;
        }

      bool displayWebcamActivated()
        {
          return displayWebcam;
        }

      bool isInteractive()
        {
          return interactive;
        }

      bool showFrame()
        {
          return displayFrame;
        }

      friend std::ostream& operator<<( std::ostream& os, const motionInterface& I );


      pthread_t th;
      bool tresholdActivated;
      int  sizeOfMask;
      bool diffActivated;
      int treshold;
      int numberOfIgnoredFrames;
      bool centerActivated;
      int theDilatation;
      int theErosion;
      bool simulationActivated;
      bool displayWebcam;
      bool interactive;
      bool displayFrame;

      };

}
#endif



