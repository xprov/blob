#ifndef __SHARED_DATA_H__
#define __SHARED_DATA_H__

#include <sys/ipc.h>
#include <sys/shm.h>
#include <semaphore.h>

/**
 * An object of class shared_data is designed to communicate data in an asynchronous way between a
 * process and his forked child.
 *
 * It implements the simplest communication scheme possible where data integrety is ensured by a
 * semaphore.
 *
 * Usage: 
 *
 *  1. Define an instance of shared_data<T>.
 *  2. Call fork()
 *  3. Use function read() and write() in order to read/write in the shared memory.
 *  4. Enjoy !
 */

template< typename T >
class shared_data
{

public :
  shared_data();
  ~shared_data();
  void write( const T & t );
  T read() const;


private :
  typedef struct
    {
      T t;
      sem_t mutex;
      pid_t fathers_pid;
    } shared_load;
  void set_father_pid();
  pid_t get_father_pid();

private :
  shared_load *ss;
};

#include "shared_data.ih"

#endif
