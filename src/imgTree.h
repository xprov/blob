
#ifndef __IMG_TREE_H__
#define __IMG_TREE_H__


#include <opencv2/highgui.hpp>
#include <opencv2/core/types_c.h>
#include <iostream>
#include <set>

/**
 * An image is decomposed recursively forming a quadtree where each 
 * node knows the average color of the region it defines.
 *
 * The for sons are identified as :
 *
 *    |---------------|     |---------------|
 *    |       |       |     |       |       |
 *    |  00   |  10   |     |   0   |   2   |
 *    |       |       |     |       |       |
 *    |---------------| ->  |---------------|
 *    |       |       |     |       |       |
 *    |  01   |  11   |     |   1   |   3   |
 *    |       |       |     |       |       |
 *    |---------------|     |---------------|
 */

namespace blob {

    class Node
      {
    public:
      Node( int r0, int c0, int r1, int c1, bool final, cv::Vec3b color ) :
        r0(r0), c0(c0), r1(r1), c1(c1), closed(true), final(final), color(color)
        { 
          for ( int i=0; i<4; ++i )
            sons[i] = NULL;
          center = cv::Point2d( ( c0+c1)/2, (r0+r1)/2 );
          radius = std::max( 1.0, ((double) std::min( c1-c0, r1-r0 ) ) / 2.0  );
          //center = cv::Point2d( ( c1), (r1) );
          //radius = std::max( 1.0, ((double) std::min( c1-c0, r1-r0 ) ) / 1.0  );
        }

      bool isInside( cv::Point2d p )
        {
          return norm( p - center ) <= radius;
          //return ( c0 <= c ) && ( c <= c1 ) && ( r0 <= r ) && ( r <= r1 );
        }

      //bool isInsideCircle( cv::Point p )
      //  {
      //    double c_x =  ( ( double) ( c0 + c1 ) ) / 2.0;
      //    double c_y =  ( ( double) ( c0 + c1 ) ) / 2.0;
      //  }

      //int radius( )
      //  {
      //    return std::min( c1-c0, r1-r0 ) / 2;
      //  }

      int r0, c0, r1, c1;
      cv::Point2d center;
      double radius;
      bool closed;
      bool final;
      Node * sons[4];
      cv::Vec3b color;
      };

    std::ostream& operator<<( std::ostream& os, Node & n );

    class imgTree
      {

    public :

      imgTree( const cv::Mat& img, int min_radius );

      std::set<Node*> * getVisible()
        { return &visible; }

      int nrows()
        { return theImage.rows; }

      int ncols()
        { return theImage.cols; }

      void blowNode( Node * n );

      Node * getRoot()
        { return root; }


    protected :

      const cv::Mat& theImage;

      Node * buildNode( int i0, int j0, int i1, int j1 );
      void buildTree();

      Node * root;

      std::set<Node*> visible;

      int minimal_radius;

      };

}

#endif
