
#include <unistd.h>
#include "motionInterface.h"


using namespace std;
using namespace blob;

namespace blob {
    ostream& operator<<( ostream& os, const motionInterface& I )
      {
        os << " 1. Seuil activé : " << I.tresholdActivated << "\n";
        os << " 2. Valeur du seuil : " << I.treshold<< "\n";
        os << " 3. Taille du masque ( 0 pour désactivé ): " << I.sizeOfMask << "\n";
        os << " 4. Soustraction activée : " << I.diffActivated << "\n";
        os << " 5. Nombre de cadres ignorés : " << I.numberOfIgnoredFrames << "\n";
        os << " 6. Afficher le centre : " << I.centerActivated << "\n";
        os << " 7. Dilatation ( 0 pour désactiver ) : " << I.theDilatation << "\n";
        os << " 8. Erosion ( 0 pour désactiver ) : " << I.theErosion << "\n";
        os << " 9. Mode simulation : " << I.simulationActivated << "\n";
        os << " 10. Affiche la webcam : " << I.displayWebcam << "\n";
        os << " 11. Affiche le cadre : " << I.displayFrame;
        return os;
      }
}

void displayMenu( motionInterface* I )
{
  cout << "État actuel : \n";
  cout << *I << endl;
}

void control( motionInterface* I )
{
  // Diff may not be activated before at least 2 frames have been captured.
  // But I want diffActivated by default...
  usleep(500000);
  I->diffActivated = true;
  // This is ugly... but should do the work for now... 
  // TODO : fix this !

  if ( I->interactive )
    {

      int i;
      usleep(  500000 );
      displayMenu( I );
      while ( cin >> i )
        {
          switch ( i )
            {
            case 1 :
              I->tresholdActivated = (I->tresholdActivated) ? false : true;
              break;
            case 2 :
              cout << "Nouveau seuil : " ;
              cin >> i;
              I->treshold = i;
              break;
            case 3 :
              cout << "Taille du masque ( doit être impair ): " ;
              cin >> i;
              while ( i % 2 == 0 )
                {
                  cout << "La taille du masque doit être impaire : ";
                  cin >> i;
                }
              I->sizeOfMask = i;
              break;
            case 4 :
              I->diffActivated = ! I->diffActivated;
              break;
            case 5 :
              cout << "Nombre ignorés : " ;
              cin >> i;
              I->numberOfIgnoredFrames = i;
              break;
            case 6 :
              I->centerActivated = ! I->centerActivated;
              break;
            case 7 :
              cout << "Nouvelle dilatataion : ";
              cin >> I->theDilatation;
              break;
            case 8 :
              cout << "Nouvelle érosion : ";
              cin >> I->theErosion;
              break;
            case 9 :
              I->simulationActivated = ! I->simulationActivated;
              break;
            case 10 :
              I->displayWebcam = ! I->displayWebcam;
              break;
            case 11 :
              I->displayFrame = ! I->displayFrame;
              break;
            default :
              cout << "Choix invalide\n" << endl;;
            }
          cout << endl;
          displayMenu( I );
        }
    }
}

motionInterface::motionInterface( )
{
}

void motionInterface::Start( bool interac) 
{
  interactive = interac;
  tresholdActivated = true;
  sizeOfMask = 9;
  diffActivated = false;
  treshold = 20;
  numberOfIgnoredFrames = 0;
  centerActivated = true;
  theDilatation = 20;
  theErosion = 16;
  simulationActivated = true;
  displayWebcam = false;
  displayFrame = true;

  pthread_create( &th, NULL, (void* (*)(void*)) &control, this );
}

//void motionInterface::Start()
//{
//}

motionInterface::~motionInterface()
{
  //pthread_kill( th, 0 );
}



