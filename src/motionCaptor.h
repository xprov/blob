
#ifndef __MOTION_CAPTOR_H__
#define __MOTION_CAPTOR_H__

#include <unistd.h>
#include <pthread.h>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
// #include "opencv2/gpu/gpu.hpp"
#include "motionInterface.h"
#include "Frame.h"
#include "shared_data.h"


#define CAPTION_FPS 30

namespace blob {

    typedef shared_data<cv::Point2d> shared_point;

    class motionCaptor {
    public :

      motionCaptor( int smoothness, bool interactive, shared_point * p, int caption_width, int caption_height );
      void getActualPosition( cv::Point2d & circle, cv::Point2d & rectangle1, cv::Point2d & rectangle2 );

      int nrows() { return rows; }
      int ncols() { return cols; }

      motionInterface * getInterface()
        { return &I; }


    private:
      shared_point * output_position;
      int points_avg;
      cv::Point * lastPos;
      int posInPos;
      pthread_mutex_t pos_array_protection;

      motionInterface I;
      cv::VideoCapture capture;
      int cols;
      int rows;
      Frame *myFrame;

      void captor_func();


    public :
      void new_position( cv::Point p );
      void display( cv::Mat frame );
      void medval( cv::Mat f, int & medR, int & medC );
      void getFrameCorners( cv::Point2d & , cv::Point2d & );

      friend void captor_func ( motionCaptor * mc );
    };
}

#endif
